package com.example.u2_practica_1.PracticaIntegradora

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.u2_practica_1.R
import kotlinx.android.synthetic.main.activity_practica_integradora.*

class PracticaIntegradoraActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica_integradora)

        cardViewSeoul.setOnClickListener{
            val intent = Intent(this, ClimaCiudadActivity::class.java).apply{
                putExtra("ciudad","Seoul")
            }
            startActivity(intent)
        }
        cardViewMadrid.setOnClickListener{
            val intent = Intent(this, ClimaCiudadActivity::class.java).apply {
                putExtra("ciudad","Madrid")
            }
            startActivity(intent)
        }
        cardViewChihuahua.setOnClickListener{
            val intent = Intent(this, ClimaCiudadActivity::class.java).apply{
                putExtra("ciudad","Chihuahua")
            }
            startActivity(intent)
        }
    }
}
package com.example.u2_practica_1.practica5

import android.media.Image
import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable

data class platillo(var nombre: String?, var precio : Int, var imagen : Int) :
    Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(nombre)
        parcel.writeInt(precio)
        parcel.writeInt(imagen)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<platillo> {
        override fun createFromParcel(parcel: Parcel): platillo {
            return platillo(parcel)
        }

        override fun newArray(size: Int): Array<platillo?> {
            return arrayOfNulls(size)
        }
    }
}
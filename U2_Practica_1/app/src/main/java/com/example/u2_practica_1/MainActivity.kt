package com.example.u2_practica_1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.u2_practica_1.Practica1.Practica1Activity
import com.example.u2_practica_1.Practica2.Practica2Activity
import com.example.u2_practica_1.Practica3.Practica3_Bundle
import com.example.u2_practica_1.PracticaIntegradora.PracticaIntegradoraActivity
import com.example.u2_practica_1.practica4.Practica4Activity
import com.example.u2_practica_1.practica5.Practica5Activity
import com.example.u2_practica_1.practica6.Practica6Activity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var boton1 : Button = findViewById(R.id.btn_practica1)
        var boton2 : Button = findViewById(R.id.btnPractica2)
        var boton3 : Button = findViewById(R.id.btnPractica3)
        var boton4 : Button = findViewById(R.id.btnPractica4)
        var boton5 : Button = findViewById(R.id.btnPractica5)

        boton1.setOnClickListener {
            val intent = Intent(this, Practica1Activity::class.java)
            startActivity(intent)
        }
        boton2.setOnClickListener {
            val intent = Intent(this, Practica2Activity::class.java)
            startActivity(intent)
        }
        boton3.setOnClickListener {
            val intent = Intent(this, Practica3_Bundle::class.java)
            startActivity(intent)
        }
        boton4.setOnClickListener {
            val intent = Intent(this, Practica4Activity::class.java)
            startActivity(intent)
        }
        boton5.setOnClickListener {
            val intent = Intent(this, Practica5Activity::class.java)
            startActivity(intent)
        }
        btnPractica6.setOnClickListener {
            val intent = Intent(this, Practica6Activity::class.java)
            startActivity(intent)
        }
        btnIntegradora.setOnClickListener {
            val intent = Intent(this, PracticaIntegradoraActivity::class.java)
            startActivity(intent)
        }
    }
}
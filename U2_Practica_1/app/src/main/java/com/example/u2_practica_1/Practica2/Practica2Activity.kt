package com.example.u2_practica_1.Practica2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.TextView
import com.example.u2_practica_1.R

const val DURATION_RESULT = 3000

class Practica2Activity : AppCompatActivity() {

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == RESULT_OK){
            when(requestCode){
                DURATION_RESULT -> {
                    if(data != null){
                        val minutes = data.getIntExtra("DURATION",-1)
                        var tvSelected : TextView = findViewById(R.id.tvSelected)
                        if (minutes >=60){
                            tvSelected.text = resources.getQuantityString(R.plurals.pluralsHours, minutes/60, minutes/60)
                        }else{
                            tvSelected.text = "$minutes minutes"
                        }

                    }
                }
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica2)

        var selector : LinearLayout = findViewById(R.id.llSelector)

        selector.setOnClickListener{
            val intent = Intent(this, DurationSelectorActivity::class.java)
            startActivityForResult(intent, DURATION_RESULT)
        }
    }
}
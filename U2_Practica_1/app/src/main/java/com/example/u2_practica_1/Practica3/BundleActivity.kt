package com.example.u2_practica_1.Practica3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.u2_practica_1.R

class BundleActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bundle)

        var tvInfo : TextView = findViewById(R.id.tvInfo)

        var bundle = Bundle()
        bundle = intent.extras ?: Bundle()
        val nameBundle = bundle.getString("NOMBRE")
        val apellidoBundle = bundle.getString("APELLIDO")
        val edadBundle = bundle.getInt("EDAD")
        val salarioBundle = bundle.getInt("SALARIO")
        val gender = bundle.getString("GENDER")
        tvInfo.text = String.format(getString(R.string.info), nameBundle, apellidoBundle, edadBundle, salarioBundle, gender)

    }
}
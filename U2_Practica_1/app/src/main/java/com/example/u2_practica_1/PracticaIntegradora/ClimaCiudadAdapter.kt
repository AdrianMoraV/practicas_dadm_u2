package com.example.u2_practica_1.PracticaIntegradora

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.u2_practica_1.R
import kotlinx.android.synthetic.main.item_citie_timezone.view.*
import kotlinx.android.synthetic.main.item_clima_ciudad.view.*
import java.text.SimpleDateFormat
import java.util.*

class ClimaCiudadAdapter(private val listener: (weather.Daily.DailyInfo)-> Unit):
    RecyclerView.Adapter<ClimaCiudadViewHolder>() {

    private val lista = mutableListOf<weather.Daily.DailyInfo>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClimaCiudadViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_clima_ciudad, parent, false)
        return ClimaCiudadViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ClimaCiudadViewHolder, position: Int) {
        holder.setData(lista[position], listener)
    }

    override fun getItemCount(): Int {
        return lista.size
    }

    fun setList(list: List<weather.Daily.DailyInfo>){
        this.lista.addAll(list)
        notifyDataSetChanged()
    }
}
class ClimaCiudadViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

    fun setData(
        item: weather.Daily.DailyInfo,
        listener: (weather.Daily.DailyInfo) -> Unit
    ){
        itemView.apply {
            val dia = getDayOfTheWeek((item.time.times(1000)))
            tvDiarv.text = ""+dia
            imgDiarv.setImageResource(getWeatherIcon(item.icon))
            tvMaxrv.text="Max.${item.temperatureMax}°"
            tvMinrv.text="Min.${item.temperatureMin}°"
            setOnClickListener{
                listener.invoke(item)
            }

        }

    }
}
fun getDayOfTheWeek(time: Long): String{
    val date = Date(time)
    val format = SimpleDateFormat("EEEE, d", Locale.getDefault())
    return format.format(date)
}
fun getWeatherIcon(name: String): Int{
    return when(name) {
        "clear-day" -> R.drawable.wic_clear_day
        "clear-night" -> R.drawable.wic_clear_night
        "rain" -> R.drawable.wic_rain
        "snow" -> R.drawable.wic_snow
        "wind" -> R.drawable.wic_wind
        "fog" -> R.drawable.wic_fog
        "sleet" -> R.drawable.wic_sleet
        "cloudy" -> R.drawable.wic_cloudy
        "partly-cloudy-day" -> R.drawable.wic_partly_cloudy_day
        "partly-cloudy-night" -> R.drawable.wic_partly_cloudy_night
        "hail" -> R.drawable.wic_hail
        "thunderstorm" -> R.drawable.wic_thunderstorm
        "tornado" -> R.drawable.wic_tornado
        else -> -1
    }
}
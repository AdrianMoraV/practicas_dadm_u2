package com.example.u2_practica_1.practica5

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import com.example.u2_practica_1.R
import kotlinx.android.synthetic.main.activity_platillo_selector2.*

class PlatilloSelectorActivity : AppCompatActivity() {

    var lista = mutableListOf<platillo>()

    private val adapter by lazy{
        PlatillosAdapter{ platillo, pos ->
            intent.putExtra("PLATILLO", platillo)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_platillo_selector2)
        //val list = intent.extras?.getSerializable("platillo") as MutableList<platillo>

        lista.add(platillo("Burrito",10,R.drawable.burritos))
        lista.add(platillo("Crepa",45,R.drawable.crepa))
        lista.add(platillo("Hamburguesa",70,R.drawable.hamburguesa))
        lista.add(platillo("Montado",50,R.drawable.montado))
        lista.add(platillo("Tacos",40,R.drawable.tacos))
        lista.add(platillo("Torta",30,R.drawable.torta))


        rvPlatillos.adapter = adapter
        adapter.setList(lista)

    }
}
package com.example.u2_practica_1.Practica2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.u2_practica_1.R

class DurationAdapter(private val listener: (Int) -> Unit): RecyclerView.Adapter<durationAdapterViewHolder>() {

    private var list = mutableListOf<Int>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): durationAdapterViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
        return durationAdapterViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: durationAdapterViewHolder, position: Int) {
        holder.setData(list[position], listener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setList(list: List<Int>){
        this.list.addAll(list)
        notifyDataSetChanged()
    }
}
class durationAdapterViewHolder(itemView : View): RecyclerView.ViewHolder(itemView){

    fun setData(duration: Int, listener : (Int) -> Unit){
        itemView.apply {
            var tvMinutes : TextView = findViewById(R.id.tvMinutes)
            var horas = 0
            if(duration / 60 > 0){
                horas = duration / 60
                tvMinutes.text = context.resources.getQuantityString(R.plurals.pluralsHours, horas, horas)
            }else{
                tvMinutes.text = "$duration minutos"
            }



            setOnClickListener{
                listener.invoke(duration)
            }
        }
    }
}
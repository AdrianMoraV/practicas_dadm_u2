package com.example.u2_practica_1.PracticaIntegradora

import com.example.u2_practica_1.R
import com.example.u2_practica_1.practica6.CitiesAndTimezones
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class weather(jsonObject: JSONObject) {


    var daily: Daily
    //var dias: ArrayList<Daily.DailyInfo> = arrayListOf()
    //var dailyInfo : Daily.DailyInfo

    init{
        val dailyJo = jsonObject.getJSONObject("daily")
        //val currentlyJo = jsonObject.getJSONObject("currently")
        daily = Daily(dailyJo)

        //currently = Currently(currentlyJo)
        //val dataJa = dailyJo.getJSONObject("data")
        //dailyInfo = Daily.DailyInfo(dataJa)
        /*for (i in 0 until dataJa.length()){
            val dataJo = dataJa.getJSONObject(i)
            dias.add(Daily.DailyInfo(dataJo))
        }*/


    }
    class Daily(dailyJo: JSONObject){
        //var data: ArrayList<DailyInfo> = arrayListOf()
        var data = arrayListOf<DailyInfo>()
        //var dailyInfo: DailyInfo
        var summary=""
        var icon = ""
        init{
            //dailyInfo = DailyInfo()
            summary  = dailyJo.getString("summary")
            icon = dailyJo.getString("icon")
            val dataJa = dailyJo.getJSONArray("data")
            for (i in 0 until dataJa.length()){
                val dataJo = dataJa.getJSONObject(i)
                data.add(DailyInfo(dataJo))
            }

        }
        class DailyInfo(dailyInfoJo: JSONObject){

            var time: Long =0
            var dia=""
            var summary =""
            var icon =""
            var temperatureMin: Double=0.0
            var temperatureMax: Double=0.0
            init{
                time = dailyInfoJo.getString("time").toLong()
                dia = getDayOfTheWeek(time)
                summary = dailyInfoJo.getString("summary")
                icon = dailyInfoJo.getString("icon")
                temperatureMax = dailyInfoJo.getDouble("temperatureMax")
                temperatureMin = dailyInfoJo.getDouble("temperatureMin")

            }
        }
    }

}


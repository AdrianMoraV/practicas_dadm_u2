package com.example.u2_practica_1.practica6

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.u2_practica_1.R
import kotlinx.android.synthetic.main.item_citie_timezone.view.*

class CitiesTimezonesAdapter(private val listener: (CitiesAndTimezones.CitiesTimezones)-> Unit):
    RecyclerView.Adapter<CitiesTimezonesViewHolder>() {

    private var list = mutableListOf<CitiesAndTimezones.CitiesTimezones>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CitiesTimezonesViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_citie_timezone, parent, false)
        return CitiesTimezonesViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CitiesTimezonesViewHolder, position: Int) {
        holder.setData(list[position], listener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setList(list: List<CitiesAndTimezones.CitiesTimezones>){
        this.list.addAll(list)
        notifyDataSetChanged()
    }
}
class CitiesTimezonesViewHolder(itemView : View): RecyclerView.ViewHolder(itemView){

    fun setData(
        item: CitiesAndTimezones.CitiesTimezones,
        listener: (CitiesAndTimezones.CitiesTimezones) -> Unit
    ){
        itemView.apply {
            tvCC.text = "${item.name}/${item.country}"
            tvTz.text = item.timeZoneName
            setOnClickListener{
                listener.invoke(item)
            }

        }

        }
    }

package com.example.u2_practica_1.practica4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.u2_practica_1.R

class Practica4Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica4)

        var botonAlert : Button = findViewById(R.id.btnAlert)
        var botonListAlert : Button = findViewById(R.id.btnAlertDialog)
        var botonRadio : Button = findViewById(R.id.btnRadioButton)
        var botonCheckButton : Button = findViewById(R.id.btnCheckButton)
        var botonCustom : Button = findViewById(R.id.btnCustom)

        botonAlert.setOnClickListener{
            val builder : AlertDialog.Builder = AlertDialog. Builder(this)
            builder.setMessage("Esto es un cuadro de dialogo").setTitle("HOLA").setCancelable(false)
                .setPositiveButton("Si") { dialog, which ->
                Toast.makeText(this,"Amonos", Toast.LENGTH_SHORT).show()
            }.setNegativeButton("No") { dialog, which ->
                Toast.makeText(this,"No", Toast.LENGTH_SHORT).show()
            }.setNeutralButton("Quizás") {dialog, wich ->
                    Toast.makeText(this,"Maomeno", Toast.LENGTH_SHORT).show()
                }

            val dialog : AlertDialog = builder.create()
            dialog.show()
        }

        botonListAlert.setOnClickListener {
            val colors = arrayOf("Red","Blue","Green","Yellow")
            val builder : AlertDialog.Builder = AlertDialog.Builder(this)
                .setTitle("Escoge un color")
                .setItems(colors) { dialog, which->
                    //which contiene el index del item seleccionado
                    when(which){
                        0-> Toast.makeText(this,"Red",Toast.LENGTH_SHORT).show()
                        1-> Toast.makeText(this,"Blue",Toast.LENGTH_SHORT).show()
                        2-> Toast.makeText(this,"Green",Toast.LENGTH_SHORT).show()
                        3-> Toast.makeText(this,"Yellow",Toast.LENGTH_SHORT).show()
                    }
                }
            val dialog = builder.create()
            dialog.show()
        }
        botonRadio.setOnClickListener {
            val colors = arrayOf("Red","Blue","Green","Yellow")
            val builder : AlertDialog.Builder = AlertDialog.Builder(this)
                .setTitle("Escoge un color")
                .setSingleChoiceItems(colors, 0){ dialog, which ->
                    Toast.makeText(this,"Seleccionaste: ${colors[which]}",Toast.LENGTH_SHORT).show()
                }
            val dialog = builder.create()
            dialog.show()
        }
        val selectedItems = arrayListOf<Int>()
        botonCheckButton.setOnClickListener {
            val colors = arrayOf("Red","Blue","Green","Yellow")
            val builder : AlertDialog.Builder = AlertDialog.Builder(this)
                .setTitle("Escoge un color")
                .setMultiChoiceItems(colors, null){dialog, which, isChecked ->
                    if(isChecked){
                        //Guardar el indice
                        selectedItems.add(which)
                        Toast.makeText(this, "Selected Items: ${selectedItems.size}", Toast.LENGTH_SHORT).show()
                    }else if(selectedItems.contains(which)){
                        //remover el indice
                        selectedItems.remove(which)
                        Toast.makeText(this, "Selected Items: ${selectedItems.size}", Toast.LENGTH_SHORT).show()
                    }
                }
            val dialog = builder.create()
            dialog.show()
        }
        botonCustom.setOnClickListener {

        }

    }
}
package com.example.u2_practica_1.PracticaIntegradora

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.u2_practica_1.R
import com.example.u2_practica_1.practica6.CitiesAndTimezones
import com.example.u2_practica_1.practica6.CitiesTimezonesAdapter
import kotlinx.android.synthetic.main.activity_clima_ciudad.*
import kotlinx.android.synthetic.main.activity_practica6.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.StringWriter
import java.text.SimpleDateFormat
import java.util.*

class ClimaCiudadActivity : AppCompatActivity() {

    private val adapter by lazy {
        ClimaCiudadAdapter{ selectedCity ->

        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clima_ciudad)
        val ciudad = intent.getSerializableExtra("ciudad") as String
        Toast.makeText(this, ""+ciudad, Toast.LENGTH_SHORT).show()
        when(ciudad){
            "Seoul" -> {
                imgFondo.setImageResource(R.drawable.seoul)//Clean y luego rebuild
            }
            "Madrid" -> {
                imgFondo.setImageResource(R.drawable.madrid)
            }
            "Chihuahua" -> {
                imgFondo.setImageResource(R.drawable.chihuahua)
            }
        }
        rvClimaCiudad.adapter = adapter

        val inputS = resources.openRawResource(R.raw.weather)
        val writer = StringWriter()
        val buffer = CharArray(1024)
        inputS.use{ input ->
            val reader = BufferedReader(InputStreamReader(input, "UTF-8"))
            var n: Int
            while(reader.read(buffer).also { n = it } != -1){
                writer.write(buffer, 0, n)
            }
        }

        //val weatherList = arrayListOf<weather.Daily.DailyInfo>()
        val jsonFile = JSONObject(writer.toString())
        val cityString = jsonFile.getString(""+ciudad)
        val jsonCity = JSONObject(cityString)
        //val dailyJo = jsonCity.getJSONObject("daily")

        val currently = jsonCity.getJSONObject("currently")
        val summary = currently.getString("summary")
        tvSummary.text=""+summary
        val icon = currently.getString("icon")
        imgWeatherIcon.setImageResource(getWeatherIcon(icon))
        val temperature = currently.getString("temperature")
        tvTemperatura.text=temperature+"°"
        tvCiudad.text=ciudad
        val actualWeather = weather(jsonCity)

        //weatherList.add(actualWeather.data)
        adapter.setList(actualWeather.daily.data)


    }
    fun getDayOfTheWeek(time: Long): String{
        val date = Date(time)
        val format = SimpleDateFormat("EEEE, d", Locale.getDefault())
        return format.format(date)
    }
    fun getWeatherIcon(name: String): Int{
        return when(name) {
            "clear-day" -> R.drawable.wic_clear_day
            "clear-night" -> R.drawable.wic_clear_night
            "rain" -> R.drawable.wic_rain
            "snow" -> R.drawable.wic_snow
            "wind" -> R.drawable.wic_wind
            "fog" -> R.drawable.wic_fog
            "sleet" -> R.drawable.wic_sleet
            "cloudy" -> R.drawable.wic_cloudy
            "partly-cloudy-day" -> R.drawable.wic_partly_cloudy_day
            "partly-cloudy-night" -> R.drawable.wic_partly_cloudy_night
            "hail" -> R.drawable.wic_hail
            "thunderstorm" -> R.drawable.wic_thunderstorm
            "tornado" -> R.drawable.wic_tornado
            else -> -1
        }
    }
}
package com.example.u2_practica_1.Practica1

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Switch
import androidx.core.app.ActivityCompat
import com.example.u2_practica_1.R


class Practica1Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica1)

        var botonCall: Button = findViewById(R.id.btnCall)
        var switch: Switch = findViewById(R.id.swtLlamada)
        var editSMS: EditText = findViewById(R.id.editSMS)

        botonCall.setOnClickListener {
            var editTelefono: EditText = findViewById(R.id.editTelefono)
            val telefono = editTelefono.text.toString()
            val mensaje = editSMS.text.toString()
            if(switch.isChecked){
                if(validateSmsPermissions()){
                    val uri = Uri.parse("smsto:$telefono")
                    val intent = Intent(Intent.ACTION_SENDTO, uri)
                    intent.putExtra("sms_body", "$mensaje")
                    startActivity(intent)
                }else{
                    val permission = arrayOf(android.Manifest.permission.SEND_SMS)
                    ActivityCompat.requestPermissions(this, permission, 102)
                }

            }else{
                if(validateCallPermissions()){
                    val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$telefono"))
                    startActivity(intent)
                } else{
                    val permission = arrayOf(android.Manifest.permission.CALL_PHONE)
                    ActivityCompat.requestPermissions(this, permission, 101)
                }
            }

        }

        switch.setOnCheckedChangeListener{ buttonView, isChecked ->
            if(isChecked){
                switch.text = "Mensaje"
                editSMS.visibility = View.VISIBLE
                botonCall.text = "Enviar"
            }else{
                switch.text = "Llamada"
                editSMS.visibility = View.GONE
                botonCall.text = "Llamar"
            }

        }
    }

    private fun validateCallPermissions(): Boolean{
        return ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
    }
    private fun validateSmsPermissions(): Boolean{
        return ActivityCompat.checkSelfPermission(this, android.Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED
    }

}
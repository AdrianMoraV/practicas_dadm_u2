package com.example.u2_practica_1.Practica2

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import com.example.u2_practica_1.R

class DurationSelectorActivity : AppCompatActivity() {

    var durationList = mutableListOf<Int>()

    private val adapter by lazy{
        DurationAdapter{ duration ->
            intent.putExtra("DURATION", duration)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_duration_selector)
        durationList = mutableListOf(5,10,15,20,25,30,60,120)
        var rvDuration: RecyclerView = findViewById(R.id.rvDuration)
        rvDuration.adapter = adapter
        adapter.setList(durationList)
    }

}
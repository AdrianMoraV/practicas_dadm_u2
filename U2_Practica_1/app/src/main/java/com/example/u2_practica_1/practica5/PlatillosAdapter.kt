package com.example.u2_practica_1.practica5

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.u2_practica_1.R

class PlatillosAdapter(private val listener: (platillo, Int) -> Unit): RecyclerView.Adapter<CustomAdapterViewHolder>() {

    private var list: MutableList<platillo> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomAdapterViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_platillo, parent, false)
        return CustomAdapterViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CustomAdapterViewHolder, position: Int) {
        holder.setData(list[position], position, listener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    //PARA AGREGAR VARIOS ITEMS
    fun setList(list: List<platillo>){
        this.list.addAll(list)
    }

    //PARA AGREGAR UNA SOLA (NO SE USA EN ESTA PRACTICA)
    fun addPerson(persona: platillo){
        this.list.add(persona)
        notifyItemInserted(list.size-1)
    }

}

class CustomAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

    fun setData(
        platillo: platillo,
        position: Int,
        listener: (platillo, Int) -> Unit){
        itemView.apply {

            var txtNombre: TextView = findViewById(R.id.txtNombre)
            var txtPrecio: TextView = findViewById(R.id.txtPrecio)
            var imagen: ImageView = findViewById(R.id.imgPlatillo)

            txtNombre.text = "${platillo.nombre}"
            txtPrecio.text = "Precio: $${platillo.precio}"
            imagen.setImageDrawable(ContextCompat.getDrawable(context, platillo.imagen))

            setOnClickListener {
                listener.invoke(platillo, position)
            }
        }
    }

}
/*class DurationAdapter(private val listener: (Int) -> Unit): RecyclerView.Adapter<durationAdapterViewHolder>() {

    private var list = mutableListOf<Int>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): durationAdapterViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
        return durationAdapterViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: durationAdapterViewHolder, position: Int) {
        holder.setData(list[position], listener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setList(list: List<Int>){
        this.list.addAll(list)
        notifyDataSetChanged()
    }
}
class durationAdapterViewHolder(itemView : View): RecyclerView.ViewHolder(itemView){

    fun setData(duration: Int, listener : (Int) -> Unit){
        itemView.apply {
            var tvMinutes : TextView = findViewById(R.id.tvMinutes)
            var horas = 0
            if(duration / 60 > 0){
                horas = duration / 60
                tvMinutes.text = context.resources.getQuantityString(R.plurals.pluralsHours, horas, horas)
            }else{
                tvMinutes.text = "$duration minutos"
            }



            setOnClickListener{
                listener.invoke(duration)
            }
        }
    }
}*/
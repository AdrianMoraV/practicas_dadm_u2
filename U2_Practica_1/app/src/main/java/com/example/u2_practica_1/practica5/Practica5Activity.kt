package com.example.u2_practica_1.practica5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.u2_practica_1.R
import kotlinx.android.synthetic.main.activity_practica5.*

const val PLATILLO_RESULT = 3001

class Practica5Activity : AppCompatActivity() {

    private val adaptador by lazy {
        PlatillosAdapter {persona, pos ->

        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == RESULT_OK){
            when(requestCode){
                PLATILLO_RESULT -> {
                    if(data != null){
                        var platilloSeleccionado: platillo = data.getParcelableExtra<platillo>("PLATILLO")!!
                        adaptador.addPerson(platilloSeleccionado)
                    }
                }
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica5)
        //var txtSelectPlatillo : TextView = findViewById(R.id.txtPlatillo)
        rvPreviewPlatillo.adapter = adaptador

        txtPlatillo.setOnClickListener{
            val intent = Intent(this, PlatilloSelectorActivity::class.java)
            startActivityForResult(intent, PLATILLO_RESULT)
        }
    }
}
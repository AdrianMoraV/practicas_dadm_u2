package com.example.u2_practica_1.Practica3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RadioGroup
import android.widget.Toast
import com.example.u2_practica_1.R
import java.nio.file.Files

class Practica3_Bundle : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica3_bundle)

        var gender = com.example.u2_practica_1.Practica3.Genero.MALE.name1
        var btnShow : Button = findViewById(R.id.btnMostrar)
        var editNombre : EditText = findViewById(R.id.editNombre)
        var editApellido : EditText = findViewById(R.id.editApellido)
        var editEdad : EditText = findViewById(R.id.editEdad)
        var editSalario : EditText = findViewById(R.id.editSalario)

        btnShow.setOnClickListener {
            val nombre = editNombre.text.toString()
            val apellido = editApellido.text.toString()
            val edad = editEdad.text.toString().toInt()
            val salario = editSalario.text.toString().toInt()
            if(nombre.isNotEmpty() && apellido.isNotEmpty() && edad>18 && salario>0 ){
                val bundle = Bundle()
                val intent = Intent(this, BundleActivity::class.java)
                bundle.putString("NOMBRE", nombre)
                bundle.putString("APELLIDO", apellido)
                bundle.putInt("EDAD", edad)
                bundle.putInt("SALARIO", salario)
                bundle.putString("GENDER",gender)
                intent.putExtras(bundle)
                startActivity(intent)

            }else{
                Toast.makeText(this, "Falta información por llenar",Toast.LENGTH_SHORT).show()
            }
        }

        var grupoGenero : RadioGroup = findViewById(R.id.groupGenero)
        grupoGenero.setOnCheckedChangeListener{ group, checkedId->
            when(checkedId){
                R.id.radioHombre ->{
                    gender = com.example.u2_practica_1.Practica3.Genero.MALE.name1
                }
                R.id.radioMujer ->{
                    gender = com.example.u2_practica_1.Practica3.Genero.FEMALE.name1
                }
            }
        }
    }
}